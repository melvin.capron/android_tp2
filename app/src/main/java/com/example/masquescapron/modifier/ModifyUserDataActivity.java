package com.example.masquescapron.modifier;

import android.os.Bundle;

import com.example.masquescapron.R;
import com.example.masquescapron.fields.UserFields;
import com.example.masquescapron.wrapper.CompatActivityWriterWrapper;

import java.util.HashMap;

public class ModifyUserDataActivity extends CompatActivityWriterWrapper {
    //fields that will be used in this activity
    HashMap<Integer, String> userFields = new UserFields().getUserFields();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // call the parent to fill edit fields with existing ones, and to tell what activity to show
        customOnCreate(userFields, R.layout.activity_modify);
    }

    /**
     * {@inheritDoc}
     */
    protected HashMap<Integer, String> getFields(){
        return userFields;
    }

    /**
     * {@inheritDoc}
     */
    protected int getModifyButton(){
        return R.id.sendModificationsUserData;
    }

    /**
     * {@inheritDoc}
     */
    protected int getCancelButton(){
        return R.id.sendCancelUserData;
    }
}