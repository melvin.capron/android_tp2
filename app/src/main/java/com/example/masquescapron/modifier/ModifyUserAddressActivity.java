package com.example.masquescapron.modifier;

import android.os.Bundle;

import com.example.masquescapron.R;
import com.example.masquescapron.fields.AddressFields;
import com.example.masquescapron.wrapper.CompatActivityWriterWrapper;

import java.util.HashMap;

public class ModifyUserAddressActivity extends CompatActivityWriterWrapper {
    //fields that will be used in this activity
    HashMap<Integer, String> addressFields = new AddressFields().getAddressFields();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // call the parent to fill edit fields with existing ones, and to tell what activity to show
        customOnCreate(addressFields, R.layout.activity_modify_user_address);
    }

    /**
     * {@inheritDoc}
     */
    protected HashMap<Integer, String> getFields(){
        return addressFields;
    }

    /**
     * {@inheritDoc}
     */
    protected int getModifyButton(){
        return R.id.sendModificationsUserAddress;
    }

    /**
     * {@inheritDoc}
     */
    protected int getCancelButton(){
        return R.id.sendCancelUserAddress;
    }
}