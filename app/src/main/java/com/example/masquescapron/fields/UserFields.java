package com.example.masquescapron.fields;

import com.example.masquescapron.R;

import java.util.HashMap;

public class UserFields {
    //const declaration for variables that will be passed accross activities
    public static final String USER_FIRSTNAME = "userFirstname";
    public static final String USER_LASTNAME = "userLastname";
    public static final String USER_PHONE_NUMBER = "userPhoneNumber";

    private HashMap<Integer, String> userFields;

    public UserFields() {
        this.userFields = new HashMap<>();
    }

    public HashMap<Integer, String> getUserFields() {
        //if empty, fill up the hashmap with known fields to use (once, like singleton)
        //Fill is occured ONLY when needed (not at the instanciation)
        if (userFields.isEmpty()) {
            userFields.put(R.id.firstname, UserFields.USER_FIRSTNAME);
            userFields.put(R.id.lastname, UserFields.USER_LASTNAME);
            userFields.put(R.id.phoneNumber, UserFields.USER_PHONE_NUMBER);
        }

        return this.userFields;
    }
}
