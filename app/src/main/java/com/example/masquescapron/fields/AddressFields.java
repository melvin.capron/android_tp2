package com.example.masquescapron.fields;

import com.example.masquescapron.R;

import java.util.HashMap;

public class AddressFields {
    //const declaration for variables that will be passed accross activities
    public static final String USER_HOUSE_NUMBER = "userHouseNumber";
    public static final String USER_STREET = "userStreet";
    public static final String USER_POSTAL_CODE = "userPostalCode";
    public static final String USER_CITY = "userCity";

    private HashMap<Integer, String> addressFields;

    public AddressFields(){
        this.addressFields = new HashMap<>();
    }

    public HashMap<Integer, String> getAddressFields(){
        //if empty, fill up the hashmap with known fields to use
        //Fill is occured ONLY when needed (not at the instanciation)
        if(addressFields.isEmpty()){
            addressFields.put(R.id.houseNumber, AddressFields.USER_HOUSE_NUMBER);
            addressFields.put(R.id.street, AddressFields.USER_STREET);
            addressFields.put(R.id.postalCode, AddressFields.USER_POSTAL_CODE);
            addressFields.put(R.id.city, AddressFields.USER_CITY);
        }

        return this.addressFields;
    }
}
