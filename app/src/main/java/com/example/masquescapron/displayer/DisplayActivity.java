package com.example.masquescapron.displayer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.masquescapron.R;
import com.example.masquescapron.fields.AddressFields;
import com.example.masquescapron.fields.UserFields;
import com.example.masquescapron.modifier.ModifyUserAddressActivity;
import com.example.masquescapron.modifier.ModifyUserDataActivity;
import com.example.masquescapron.wrapper.CompatActivityReaderWrapper;

import java.util.HashMap;
import java.util.Map;

public class DisplayActivity extends CompatActivityReaderWrapper {
    //declare constant for request code
    final int LAUNCH_MODIFY_USER_DATA = 1;
    final int LAUNCH_MODIFY_USER_ADDRESS = 2;

    //declare fields we are going to use all along the application
    HashMap<Integer, String> userFields = new UserFields().getUserFields();
    HashMap<Integer, String> addressFields = new AddressFields().getAddressFields();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        //Give action to modify buttons
        this.modifyActionUserData();
        this.modifyActionUserAddress();
    }

    /**
     * Generic function to add "modify" action for the lastname, firstname, phonenumber part
     */
    private void modifyActionUserData() {
        this.genericModifyButtonAction(R.id.modifyUser, userFields, ModifyUserDataActivity.class, LAUNCH_MODIFY_USER_DATA);
    }

    /**
     * Generic function to add "modify" action for the street, city, postalcode, house number part
     */
    private void modifyActionUserAddress() {
        this.genericModifyButtonAction(R.id.modifyAddress, addressFields, ModifyUserAddressActivity.class, LAUNCH_MODIFY_USER_ADDRESS);
    }

    /**
     *
     * @param requestCode given request code when the activity is launched to know who answer us
     * @param resultCode the result code
     * @param data the data given by the intent
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // when there is a result for one activity, handle it
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            HashMap<Integer, String> fieldsToUse;

            switch (requestCode) {
                case LAUNCH_MODIFY_USER_DATA:
                    //if from classic user data, point user fields
                    fieldsToUse = userFields;
                    break;
                case LAUNCH_MODIFY_USER_ADDRESS:
                    //if from address user data, point address fields
                    fieldsToUse = addressFields;
                    break;
                default:
                    //otherwise, we can't proceed the query
                    System.out.println("The given request code has not been processed.");
                    return;
            }

            //based on the given fields (and the request code), we can update the displayer with the modified values as a generic function
            for (Map.Entry<Integer, String> entry : fieldsToUse.entrySet()) {
                this.setTextViewString(entry.getKey(), data.getStringExtra(entry.getValue()));
            }
        }
    }
}