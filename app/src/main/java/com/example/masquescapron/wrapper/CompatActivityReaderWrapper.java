package com.example.masquescapron.wrapper;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Map;

public class CompatActivityReaderWrapper extends AppCompatActivity {
    /**
     *
     * @param resourceId with the form : R.id.resourceId, will be needed to find the element
     * @return String from the given text view
     */
    protected String getTextViewString(int resourceId){
        return ((TextView) findViewById(resourceId)).getText().toString();
    }

    /**
     *
     * @param resourceId with the form : R.id.resourceId, will be needed to find the element
     * @param text replace the text of the element by the new text
     */
    protected void setTextViewString(int resourceId, String text){
        ((TextView) findViewById(resourceId)).setText(text);
    }

    /**
     *
     * @param buttonResourceId with the form : R.id.resourceId, will be needed to find the element
     * @param fieldsToUse HashMap of fields to use to pass data to next activity
     * @param targetedActivity The targeted activity we want to show after pressing the modify button
     * @param requestActivityCode The given code to not overload the application
     */
    protected void genericModifyButtonAction(int buttonResourceId, final HashMap<Integer, String> fieldsToUse, final Class<?> targetedActivity, final int requestActivityCode) {
        Button clickButton = (Button) findViewById(buttonResourceId);

        clickButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent displayText = new Intent(getApplicationContext(), targetedActivity);

                //Parse fields to give results to next activity
                for (Map.Entry<Integer, String> entry : fieldsToUse.entrySet()) {
                    displayText.putExtra(entry.getValue(), getTextViewString(entry.getKey()));
                }

                startActivityForResult(displayText, requestActivityCode);
            }
        });
    }
}
