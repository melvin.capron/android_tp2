package com.example.masquescapron.wrapper;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.masquescapron.displayer.DisplayActivity;

import java.util.HashMap;
import java.util.Map;

public abstract class CompatActivityWriterWrapper extends AppCompatActivity {
    /**
     * Custom on create to fill EditText with TextView values given by the displayer activity
     *
     * @param fields             to fill
     * @param activityResourceId what activity to show
     */
    protected void customOnCreate(HashMap<Integer, String> fields, int activityResourceId) {
        setContentView(activityResourceId);

        for (Map.Entry<Integer, String> entry : fields.entrySet()) {
            this.setEditTextString(entry.getKey(), getIntent().getStringExtra(entry.getValue()));
        }

        this.modifyAction();
        this.backAction();
    }

    /**
     * @param resourceId with the form : R.id.resourceId, will be needed to find the element
     */
    private String getEditTextString(int resourceId) {
        return ((EditText) findViewById(resourceId)).getText().toString();
    }

    /**
     * @param resourceId with the form : R.id.resourceId, will be needed to find the element
     * @param text       replace the text of the element by the new text
     */
    private void setEditTextString(int resourceId, String text) {
        ((EditText) findViewById(resourceId)).setText(text);
    }

    /**
     * Generic function to generate a "modify" action
     */
    private void modifyAction() {
        Button clickButton = (Button) findViewById(this.getModifyButton());

        clickButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent displayText = new Intent(getApplicationContext(), DisplayActivity.class);

                //Parse fields to give a result to the main application
                for (Map.Entry<Integer, String> entry : getFields().entrySet()) {
                    displayText.putExtra(entry.getValue(), getEditTextString(entry.getKey()));
                }

                setResult(RESULT_OK, displayText);
                finish();
            }
        });
    }

    /**
     * Generic function to generate a "back" action
     */
    private void backAction() {
        Button clickButton = (Button) findViewById(this.getCancelButton());

        clickButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent displayText = new Intent(getApplicationContext(), DisplayActivity.class);
                setResult(RESULT_CANCELED, displayText);
                finish();
            }
        });
    }

    /**
     *
     * @return the fields to use
     */
    protected abstract HashMap<Integer, String> getFields();
    /**
     *
     * @return the modify button ID
     */
    protected abstract int getModifyButton();

    /**
     *
     * @return the back button ID
     */
    protected abstract int getCancelButton();
}
